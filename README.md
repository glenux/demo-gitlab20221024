# Enoncé

## Récupérer le dépot

* Avec la ligne de commande GIT, télécharger le contenu du dépot sur votre ordinateur, en SSH.

## Ajouter du contenu

Dans le fichier `ELEVES.md` vous ajouterez votre nom, sous la forme suivante :

```
* DELAUR, Eric
```

... en vous assurant que les noms sont bien classés dans l'ordre alphabétique
des noms de famille.

## Envoyer vos modifications sur le serveur Gitlab

* Ajouter ces modifications à votre historique local (git add + git commit)
* Récupérer les modifications faites par vos collaborateurs (git fetch + git merge / git pull)
* Gérez les conflits si besoin (editor + git commit -a)
* Envoyer vos modification sur le serveur central pour les mettre à disposition de vos collegues

![Alt Text](https://www.melenshack.fr/1v0lZSCH.gif)
